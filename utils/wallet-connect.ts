export const setConnected = async (isConnected: boolean) => {
    const walletMetaStr = localStorage.getItem("walletMeta")
    let walletMeta = { isConnected: false }
    if (walletMetaStr) {
        walletMeta = JSON.parse(walletMetaStr)
    }
    walletMeta.isConnected = isConnected
    localStorage.setItem("walletMeta", JSON.stringify(walletMeta))
}