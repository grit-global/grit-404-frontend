import { defineStore } from 'pinia'
import { toast } from 'vue3-toastify'
import { SDKProvider } from '@metamask/sdk'

export const useCommonStore = defineStore('common', {
    state  : () => ({
        pageLoading: false as boolean,
        ethereum   : undefined as SDKProvider | undefined,
        metaMask   : undefined as any,
        nft        : [] as any[]
    }),
    getters: {
        isPageLoading(state): any {
            return state.pageLoading
        },
        getEthereum(state) {
            return state.ethereum
        },
        getMetaMask(state) {
            return state.metaMask
        },
        nftData(state): any {
            return state.nft
        }
    },
    actions: {
        setPageLoading(loadingState: boolean): void {
            this.pageLoading = loadingState
        },
        showMessage(message: string, type?: 'success' | 'error' | 'warning' | 'info'): void {
            const opt: any = {
                position       : 'bottom-right',
                autoClose      : 4000,
                closeOnClick   : true,
                hideProgressBar: true,
                theme          : 'light'
            }
            if (type) {
                toast[type](message, opt)
            } else {
                toast.success(message, opt)
            }
        },
        setEtherium(data: SDKProvider): void {
            this.ethereum = data
        },
        setMetaMask(data: any): void {
            this.metaMask = data
        },
        setNftData(data: any[]): void {
            this.nft = data
        }
    }
})
