import { MetaMaskSDK, SDKProvider } from '@metamask/sdk'
import type { MetaMaskSDKOptions } from '@metamask/sdk/dist/browser/es/src/sdk'
import { setConnected } from '../utils/wallet-connect'
import { getNFTsOfOwner } from '~/utils/contracts'

export default defineNuxtPlugin(async (nuxtApp) => {
    const store = useCommonStore()
    console.debug('defineNuxtPlugin');

    // init sdk first
    const opts: MetaMaskSDKOptions = {
        dappMetadata: {
            name: 'Example Node.js Dapp',
            url: window.location.href
        }
    }
    const sdk: MetaMaskSDK = new MetaMaskSDK(opts)
    await sdk.init()
    store.setEtherium(sdk.getProvider() as SDKProvider)

    const isMetaMaskConnected = () => {
        try {
            const walletMetaStr = localStorage.getItem("walletMeta")
            if (walletMetaStr) {
                const walletMeta = JSON.parse(walletMetaStr)

                return walletMeta.isConnected
            } else {
                return false
            }

        } catch (error) {
            console.error(error)
            console.debug('metamask not connected')
            return false
        }
    }

    // handle account change
    const handleAccountsChanged = (accounts: any) => {
        console.debug(`accounts: ${accounts}`)
        if (accounts.length === 0) {
            // MetaMask is locked or the user has not connected any accounts.
            console.debug("Please connect to MetaMask.");
            store.setMetaMask({ "state": 0, "buttonText": "Connect MetaMask", "currentAccount": undefined })
            setConnected(false)

        } else if (accounts[0] !== store.getMetaMask.currentAccount) {
            // Reload your interface with accounts[0].
            store.setMetaMask({ "state": 1, "buttonText": "Disconnect MetaMask", "currentAccount": accounts[0] })
            setConnected(true)

            // get all nft
            store.setPageLoading(true)
            getNFTsOfOwner(accounts[0]).then((nfts) => {
                store.setNftData(nfts)
            }).finally(() => {
                store.setPageLoading(false)
            })
        }
    }

    const handleConnected = (_connectInfo: any) => {
        console.debug("detected connected")
        store.setMetaMask({ ...store.metaMask, "state": 1, "buttonText": "Disonnect MetaMask" })
        setConnected(true)

        getAccount()
    }

    const handleDisconnected = () => {
        console.debug("detected disconnect")
        store.setMetaMask({ "state": 0, "buttonText": "Connect MetaMask", "currentAccount": undefined })
        setConnected(false)
    }

    const getAccount = async () => {

        try {
            const accounts = await store.getEthereum?.request({ method: 'eth_requestAccounts' })
            handleAccountsChanged(accounts)
        } catch (err: any) {
            if (err.code === 4001) {
                // EIP-1193 userRejectedRequest error
                // If this happens, the user rejected the connection request.
                console.debug('Please connect to MetaMask.');
                setConnected(false);
            } else {
                console.error(err);
            }
        }
    }

    // Check metaMask state
    // First, check if sdk inited
    if (typeof store.getEthereum !== 'undefined') {
        store.setMetaMask({ "state": 0, "buttonText": "Connect MetaMask", "currentAccount": undefined })

        // check if connected
        if (isMetaMaskConnected()) {
            console.debug("Metamask is connected")

            // get account
            getAccount()

            // change stage
            store.setMetaMask({ "state": 1, "buttonText": "Disconnect MetaMask", "currentAccount": undefined })

            // then switch to our supported chain if necessary
            const sepoliaNet = {
                chainId: "0xaa36a7",
                rpcUrls: ["https://ethereum-sepolia-rpc.publicnode.com"],
                chainName: "Sepolia",
                nativeCurrency: {
                    name: "sepoliaETH",
                    symbol: "ETH",
                    decimals: 18
                },
                blockExplorerUrls: ["https://sepolia.etherscan.io"]
            }
            const avaxFujiNet = {
                chainId: "0xa869",
                rpcUrls: ["https://api.avax-test.network/ext/bc/C/rpc"],
                chainName: "Avalanche Testnet C-Chain",
                nativeCurrency: {
                    name: "AVAX",
                    symbol: "AVAX",
                    decimals: 18
                },
                blockExplorerUrls: ["https://subnets-test.avax.network/"]
            }

            // TODO: check if switched to custom chain
            // add custom network to metamask
            store.getEthereum.request({
                method: "wallet_addEthereumChain",
                params: [sepoliaNet]
            });

            // listion accounts changed
            store.getEthereum.on("accountsChanged", async (accounts: any) => { handleAccountsChanged(accounts) });
            
            
        } else {
            console.debug("Metamask is not connected")
        }
    } else {
        console.debug("Metamask sdk is not inited")
        store.setMetaMask({ "state": 0, "buttonText": "Connect MetaMask", "currentAccount": undefined })
    }
})